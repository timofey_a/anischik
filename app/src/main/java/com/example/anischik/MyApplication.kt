package com.example.anischik

import android.app.Application
import android.content.Context
import com.yandex.metrica.YandexMetrica

import com.yandex.metrica.YandexMetricaConfig
import com.yandex.metrica.push.YandexMetricaPush

const val API_key = "d5b130f4-ca33-4b32-a65f-44255178f7fe"

class MyApplication: Application() {

    companion object {
        internal lateinit var appContext: Context
    }


    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext

        // Creating an extended library configuration.
        val config = YandexMetricaConfig.newConfigBuilder(API_key).build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)

        YandexMetricaPush.init(applicationContext)

        YandexMetrica.reportEvent("my application launched")
    }
}
package com.example.anischik

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.RemoteViews
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.anischik.adapter.AppsAdapter
import com.example.anischik.adapter.toSortBy
import com.example.anischik.database.AppRepository
import com.example.anischik.interfaces.AppStorage
import com.example.anischik.interfaces.WithNavController
import com.example.anischik.models.AppEntity
import com.example.anischik.models.AppItem
import com.example.anischik.service.DownloadWallpaperService
import com.example.anischik.uiComponents.MainActivity
import com.example.anischik.uiComponents.Pref
import com.example.anischik.uiComponents.SwipeListener
import com.example.anischik.uiComponents.welcome.WelcomePage
import com.example.anischik.utils.fromPackageUri
import com.example.anischik.utils.wallpaper.WallpaperUtils
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_nav_main.*
import kotlinx.android.synthetic.main.app_bar_nav_main.*
import kotlinx.coroutines.*

interface WithBackground {
    val wallpaper: ImageView
}
class NavigationMain : AppCompatActivity(),
    AppStorage, WithNavController, WithBackground {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Default + job)
    lateinit var prefs: SharedPreferences

    private var background: Drawable? = null
    private lateinit var swipeListener: SwipeListener
    private lateinit var gestureDetector: GestureDetectorCompat

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var appRep: AppRepository

    private val myReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(this::class.java.simpleName.toString(), "Receive intent ${intent?.action}")
            when (intent?.action) {
                Intent.ACTION_PACKAGE_ADDED -> packageAdded(intent)
                Intent.ACTION_PACKAGE_REMOVED -> packageRemoved(intent)
                DownloadWallpaperService.BROADCAST_ACTION_UPDATE_WALLPAPERS ->
                    updateWallpaper()
            }
        }
    }

    companion object {
        private const val CHANNEL_ID = "111"
        const val WALLPAPER_WORK_NAME = "WallpaperWork"
        private lateinit var appsAdapter: AppsAdapter
    }

    override fun getAppsAdapter() = appsAdapter
    override fun getNavController(): NavController = findNavController(R.id.nav_host_fragment)

    override val wallpaper
    get() = layout_wallpaper

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            firstStart()
            setTheme()
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_main)

        createNotificationChannel()

        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        appRep = AppRepository(applicationContext)

        if (savedInstanceState == null) {
            makeAdapter()
        }

        updateWallpaper()

        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_launcher, R.id.nav_list
            ), drawer_layout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        swipeListener = makeSwipeListener()
        gestureDetector = GestureDetectorCompat(this, swipeListener)

        findViewById<View>(R.id.simple_push).setOnClickListener(::onClickSimplePush)
        findViewById<View>(R.id.custom_push).setOnClickListener(::onClickSimplePush)
    }

    override fun onResume() {
        val filter = IntentFilter()
        filter.apply {
            addAction(Intent.ACTION_PACKAGE_REMOVED)
            addAction(Intent.ACTION_PACKAGE_ADDED)
            addDataScheme("package")
        }

        val wallpaperFilter =
            IntentFilter().apply {
                addAction(DownloadWallpaperService.BROADCAST_ACTION_UPDATE_WALLPAPERS)
            }

        registerReceiver(myReceiver, filter)
        registerReceiver(myReceiver, wallpaperFilter)
        super.onResume()
    }

    override fun onBackPressed() {
        if (getCurFragmentId() != R.id.nav_home) {
            getNavController().navigate(R.id.action_to_gome)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(myReceiver)
        } catch (_: Exception) {}
        super.onDestroy()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (!drawer_layout.isDrawerVisible(GravityCompat.START) && gestureDetector.onTouchEvent(ev)) true
            else super.dispatchTouchEvent(ev)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_nav_main_drawer, menu)
        return false
    }

    fun getCurFragmentId(): Int? {
        return getNavController().currentDestination?.id
    }

    private fun makeSwipeListener(): SwipeListener {
        return object : SwipeListener(this@NavigationMain) {
            override fun onSwipeUp() =
                when (getCurFragmentId()) {
                    R.id.nav_home -> {
                        getNavController().navigate(R.id.action_nav_home_to_nav_launcher)
                        true
                    }
                    else -> false
                }

            override fun onSwipeDown() = false

            override fun onSwipeRight() =
                when(getCurFragmentId()) {
                    R.id.nav_launcher -> {
                        getNavController().navigate(R.id.action_nav_launcher_to_nav_home)
                        true
                    }
                    R.id.nav_list -> {
                        getNavController().navigate(R.id.action_nav_list_to_nav_launcher)
                        true
                    }
                    else -> false
                }

            override fun onSwipeLeft() =
                when(getCurFragmentId()) {
                    R.id.nav_launcher -> {
                        getNavController().navigate(R.id.action_nav_launcher_to_nav_list)
                        true
                    }
                    else -> false
                }

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun onClickLauncherImage(view: View) =
        startActivity(Intent(this, MainActivity::class.java))

    private fun firstStart(): Boolean {

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

            if (prefs.getBoolean(Pref.FIRST_START, true)) {
                val interval = PreferenceManager.getDefaultSharedPreferences(this)
                    .getString(Pref.WALLPAPER_UPDATE_FREQUENCY, Pref.WallpaperUpdateFrequency.ONE)
                WallpaperUtils.addWallpaperWork(interval.toInt())
                finish()
                startActivity(Intent(this, WelcomePage::class.java))
            }
        return false
    }

    private fun updateWallpaper() {
        Log.d(this::class.java.simpleName.toString(), "Update wallpaper")
        CoroutineScope(Dispatchers.Default).launch {
            val source = PreferenceManager.getDefaultSharedPreferences(this@NavigationMain)
                .getString(Pref.WALLPAPER_SOURCE, Pref.WallpaperSource.ONE)
            val temp = WallpaperUtils.getWallpaper(source)
            withContext(Dispatchers.Main) {
                layout_wallpaper.setImageDrawable(temp)
            }
        }
    }

    private fun setTheme() {
        val nightMode = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
            Pref.DARK_MODE,
            false
        )
        val mode = if (nightMode)
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_NO
        AppCompatDelegate.setDefaultNightMode(mode)
    }

    private fun makeAdapter() {

        appsAdapter = AppsAdapter()
        appsAdapter.updateApps = ::updateAppList
        appsAdapter.updateVariables = ::updateAdapterSorting
    }

    private fun updateAppList() {
        val int = Intent(Intent.ACTION_MAIN)
        int.addCategory(Intent.CATEGORY_LAUNCHER)

        scope.launch(Dispatchers.Default) {

            val appsInfos = packageManager.queryIntentActivities(int, 0)
            val myAppPackage = application.packageName

            val appList = appsInfos.map {
                val appPackage = it.activityInfo.packageName
                val label = it.loadLabel(packageManager).toString()
                val icon = it.loadIcon(packageManager)
                val pi = packageManager.getPackageInfo(appPackage, 0)
                val entity = appRep.getApp(appPackage)

                    AppItem(appPackage, label, icon,
                        installTime = pi.firstInstallTime,
                        updateTime = pi.lastUpdateTime,
                        launchAmount = entity?.amount ?: 0,
                        lastStart = entity?.lastUse ?: 0
                    )
            }.toMutableList()
                .filter { it.appPackage != myAppPackage }

            val listJob = launch(Dispatchers.Main) {
                    appsAdapter.submitList(appList)
            }

            listJob.join()
            appRep.updateFromList(
                appList.map { AppEntity(it.appPackage, it.label) }
            )
        }
    }

    private fun onClickSimplePush(view: View) {
        with(NotificationManagerCompat.from(this)) {

            val intent = Intent(this@NavigationMain, NavigationMain::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent = PendingIntent.getActivity(this@NavigationMain, 0, intent, 0)
            val notification: Notification

            if (view.id == R.id.simple_push) {

                notification = NotificationCompat.Builder(this@NavigationMain, CHANNEL_ID)
                    .setSmallIcon(R.drawable.small_icon)
                    .setContentTitle("обычный пуш")
                    .setContentText("ничего необычного")
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .build()
            } else {
                val rv = RemoteViews(packageName, R.layout.custom_notification)
                rv.setTextViewText(R.id.tvTitle, "кастомный пуш")
                rv.setTextViewText(R.id.tvContent, "что-то необычное")
                notification = NotificationCompat.Builder(this@NavigationMain, CHANNEL_ID )
                    .setContent(rv)
                    .setSmallIcon(R.drawable.small_icon)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .build()
            }

            notify(CHANNEL_ID.toInt(), notification)
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =  NotificationChannel(
                CHANNEL_ID,
                "anischikNotifications",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }
    }

    private fun updateAdapterSorting() {
        appsAdapter.reverseSorting = prefs.getBoolean(Pref.REVERSE_SORTING, false)
        appsAdapter.sortBy = (prefs.getString(Pref.APPS_SORTING, Pref.AppsSorting.NO_SORT) as String).toSortBy()
    }

    private fun packageAdded(intent: Intent) {
        YandexMetrica.reportEvent("app installed")
        appsAdapter.update()
    }

    private fun packageRemoved(intent: Intent) {
        //get package name without "package:"
        val appPackage = intent.data?.fromPackageUri()
        YandexMetrica.reportEvent("app deleted")
        if(appPackage != null) {
            appsAdapter.removeApp(appPackage)
            appsAdapter.update()
            appRep.deleteApp(appPackage)
        }
    }
}


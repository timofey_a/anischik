package com.example.anischik

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import com.example.anischik.uiComponents.Pref
import com.yandex.metrica.push.YandexMetricaPush





class SilentPushReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val payload = intent?.getStringExtra(YandexMetricaPush.EXTRA_PAYLOAD) ?: return
        val prefs = PreferenceManager.getDefaultSharedPreferences(context?.applicationContext ?: return)

        prefs.edit().putString(Pref.SILENT_PUSH_VALUE, payload).apply()
    }
}
package com.example.anischik.adapter

import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.anischik.R
import com.example.anischik.adapter.holders.LauncherViewHolder
import com.example.anischik.adapter.holders.ListViewHolder
import com.example.anischik.models.AppItem
import com.example.anischik.uiComponents.Pref

fun String.toSortBy(): AppsAdapter.SortBy =
    when (this) {
        Pref.AppsSorting.BY_LABEL -> AppsAdapter.SortBy.LABEL
        Pref.AppsSorting.BY_LAUNCH_AMOUNT -> AppsAdapter.SortBy.LAUNCH_AMOUNT
        Pref.AppsSorting.BY_INSTALL_TIME -> AppsAdapter.SortBy.INSTALL_TIME
        Pref.AppsSorting.BY_UPDATE_TIME -> AppsAdapter.SortBy.UPDATE_TIME
        else -> AppsAdapter.SortBy.NO_SORT
    }

class AppsAdapter :
    ListAdapter<AppItem, ViewHolder>(DiffCall()) {


    var updateApps: () -> Unit = {}
    var updateVariables: () -> Unit = {}

    var sourceList = emptyList<AppItem>()

    var sortBy: SortBy = SortBy.LABEL
    var reverseSorting: Boolean = false

    var onItemClick: (View?, Int) -> Unit = { _, _ -> }
    var onItemLongClick: (View?, Int) -> Boolean = { _, _ -> false }
    var layoutType: ItemHolderType = ItemHolderType.LAUNCHER

    companion object {
        var packageManager: PackageManager? = null
    }

    enum class SortBy {
        NO_SORT,
        LABEL,
        INSTALL_TIME,
        LAUNCH_AMOUNT,
        UPDATE_TIME,
        LAST_START
    }

    fun update() {
        updateVariables()
        updateApps()
        reSort()
        notifyDataSetChanged()
    }

    fun reSort(pref: String? = null, reverse: Boolean? = null) {
        if (pref != null){
            sortBy = pref.toSortBy()
        }
        if (reverse != null) {
            reverseSorting = reverse
        }
        submitList(sourceList)
    }

    override fun submitList(list: List<AppItem>?) {
        sourceList = list ?: listOf()
        if (list != null) {
            val mutabList = list.toMutableList()
            mutabList.sortWith(Comparator {a, b -> a.compareTo(b, reverseSorting)})
            super.submitList(mutabList)
        }
    }

    fun removeApp(appPackage: String) {
        submitList(sourceList.mapNotNull { if (it.appPackage != appPackage) it else null })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            ItemHolderType.LIST.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_app_list, parent, false)
                ListViewHolder(view)
            }
            ItemHolderType.LAUNCHER.value -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_app_grid, parent, false)
                LauncherViewHolder(view)
            }
            else -> throw IllegalArgumentException("unknown type")
        }
    }

    fun AppItem.compareTo(second: AppItem): Int =
        when (sortBy) {
            SortBy.NO_SORT -> 0
            SortBy.INSTALL_TIME -> installTime.compareTo(second.installTime) * (-1)
            SortBy.UPDATE_TIME -> updateTime.compareTo(second.updateTime) * (-1)
            SortBy.LABEL -> label.compareTo(second.label)
            SortBy.LAUNCH_AMOUNT -> launchAmount.compareTo(second.launchAmount) * (-1)
            SortBy.LAST_START -> lastStart.compareTo(second.lastStart) * (-1)
        }

    fun AppItem.compareTo(second: AppItem, reverse: Boolean): Int =
        this.compareTo(second) * if (reverse) -1 else 1

    override fun getItemViewType(position: Int): Int = layoutType.value

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item: AppItem = getItem(position)
        when (holder) {
            is LauncherViewHolder -> holder.bind(item, onItemClick, onItemLongClick)
            is ListViewHolder -> holder.bind(item, onItemClick, onItemLongClick)
        }
    }
}

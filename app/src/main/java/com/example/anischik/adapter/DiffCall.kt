package com.example.anischik.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.anischik.models.AppItem

class DiffCall : DiffUtil.ItemCallback<AppItem>() {


    override fun areItemsTheSame(oldItem: AppItem, newItem: AppItem): Boolean {
        return (oldItem.appPackage == newItem.appPackage) and (oldItem.label == newItem.label)
    }

    override fun areContentsTheSame(oldItem: AppItem, newItem: AppItem): Boolean {
        return (oldItem.appPackage == newItem.appPackage) and (oldItem.label == newItem.label)
    }
}
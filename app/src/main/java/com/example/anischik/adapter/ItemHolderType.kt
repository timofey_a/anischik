package com.example.anischik.adapter

enum class ItemHolderType(val value: Int) {
    LIST(0),
    LAUNCHER(1)
}
package com.example.anischik.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.models.AppItem
import kotlinx.android.synthetic.main.item_app_grid.view.*

class LauncherViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var application : AppItem? = null

    fun bind(appItem: AppItem,
             onItemClick: ((View?, Int) -> Unit) = { _, _ -> },
             onItemLongClick: ((View?, Int) -> Boolean) = { _, _ -> false}
    ) {
        application = appItem
        itemView.icon.setImageDrawable(appItem.icon)
        itemView.text_view.text = appItem.label

        itemView.apply {
            card_view.setOnClickListener { onItemClick(it, layoutPosition) }
            text_view.setOnClickListener { onItemClick(it, layoutPosition) }
            card_view.setOnLongClickListener { onItemLongClick(it, layoutPosition) }
            text_view.setOnLongClickListener { onItemLongClick(it, layoutPosition) }
        }
    }
}
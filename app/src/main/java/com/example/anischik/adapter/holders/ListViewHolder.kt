package com.example.anischik.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.models.AppItem
import kotlinx.android.synthetic.main.item_app_list.view.*

class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var application: AppItem? = null

    fun bind(appItem: AppItem,
             onItemClick: ((View?, Int) -> Unit) = { _, _ -> },
             onItemLongClick: ((View?, Int) -> Boolean) = { _, _ -> false}
    ) {
        application = appItem
        itemView.icon.setImageDrawable(appItem.icon)
        itemView.text_title.text = appItem.label
        itemView.text_secondary.text = appItem.appPackage

        itemView.setOnClickListener { onItemClick(it, layoutPosition) }
        itemView.setOnLongClickListener { onItemLongClick(it, layoutPosition) }
    }
}
package com.example.anischik.database

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.anischik.models.AppEntity
import com.example.anischik.models.AppEntity.Companion.APP_ENTITY_TABLE
import com.example.anischik.models.AppEntity.Companion.LAST_USE
import com.example.anischik.models.AppEntity.Companion.PACKAGE_NAME
import com.example.anischik.models.AppEntity.Companion.STARTS_AMOUNT
import com.example.anischik.models.FeaturedEntity
import com.example.anischik.models.FeaturedEntity.Companion.FEATURED_APPS_TABLE
import com.example.anischik.models.FeaturedEntity.Companion.INTENT
import com.example.anischik.models.FeaturedEntity.Companion.LABEL
import com.example.anischik.models.FeaturedEntity.Companion.POSITION
import com.example.anischik.models.FeaturedEntity.Companion.URI

@Dao
interface AppDao {


    @Query("SELECT * from $APP_ENTITY_TABLE")
    fun getApps(): LiveData<List<AppEntity>>

    @Query("SELECT * from $APP_ENTITY_TABLE where $PACKAGE_NAME = :appPackage")
    fun getEntity(appPackage: String): AppEntity

    @Query("SELECT * from $FEATURED_APPS_TABLE order by $POSITION ASC")
    fun getFeaturedApps(): LiveData<List<FeaturedEntity>>

    @Query("SELECT * from $FEATURED_APPS_TABLE where $LABEL = :label and $URI = :uri LIMIT 1")
    fun getFeatured(uri: String, label: String): FeaturedEntity?

    @Query("UPDATE $FEATURED_APPS_TABLE set $PACKAGE_NAME = :appPackage, $LABEL = :label, $INTENT = :intent, $URI = :uri where $POSITION = :pos")
    fun updateFeatured(label: String?,intent: String?, uri: String?, appPackage: String?, pos: Int?): Int

    @Query("SELECT $POSITION from $FEATURED_APPS_TABLE where $LABEL ISNULL  order by $POSITION ASC LIMIT 1")
    fun getEmptyFeaturePos(): Int?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFeatured(featuredApp: FeaturedEntity)

    @Transaction
    fun setFeatured(f: FeaturedEntity): Boolean {
        val tempF = if ((f.uri != null) and (f.label != null))
            getFeatured(f.uri!!, f.label!!)
        else null
        if ((f.position == null) and (tempF != null))
            return false
        f.position = f.position ?: getEmptyFeaturePos()
        if (tempF != null) {
            updateFeatured(null, null, null, null, tempF.position)
        }
        if (f.position != null) {
            if (updateFeatured(f.label, f.intent, f.uri, f.packagename, f.position) == 0) {
                insertFeatured(f)
            }
            return true
        }
        return false
    }

    @Query("DELETE from $FEATURED_APPS_TABLE where $URI = :uri and $LABEL = :label")
    fun removeFromFeatured(label: String, uri: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertApp(app: AppEntity?)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertApps(packageNames: List<AppEntity>)

    @Query("UPDATE $APP_ENTITY_TABLE set $LAST_USE = :time where $PACKAGE_NAME = :appPackage")
    fun setUsed(appPackage: String, time: Long)

    @Query("SELECT * from $APP_ENTITY_TABLE where $LAST_USE > 0 order by $LAST_USE DESC LIMIT :amount")
    fun getUsed(amount: Int): List<AppEntity>

    @Query("SELECT * from $APP_ENTITY_TABLE where $LAST_USE > 0 order by $LAST_USE DESC")
    fun getUsed(): List<AppEntity>

    @Delete
    fun deleteApp(app: AppEntity?)

    @Delete
    fun deleteApps(apps: List<AppEntity?>)

    @Query("SELECT $POSITION from $FEATURED_APPS_TABLE where $PACKAGE_NAME like :removedPackageName")
    fun featuredByPackage(removedPackageName: String): List<Int>

    @Query("DELETE from $APP_ENTITY_TABLE where $PACKAGE_NAME like :removedPackageName")
    fun deleteEntityApp(removedPackageName: String)

    @Transaction
    fun deleteApp(removedPackageName: String) {
        deleteEntityApp(removedPackageName)
        for (pos in featuredByPackage(removedPackageName)) {
            updateFeatured(null, null, null, null, pos)
        }
    }

    @Query("DELETE from $APP_ENTITY_TABLE")
    fun deleteAll()

    @Query("SELECT * from $APP_ENTITY_TABLE where $PACKAGE_NAME not in (:appPackages)")
    fun selectNotIn(appPackages: List<String?>): List<AppEntity>

    @Transaction
    fun updateFromList(appPackages: List<AppEntity>) {
        insertApps(appPackages)
        deleteApps(selectNotIn(appPackages.map {it.packageName}))
    }

    @Query("UPDATE $APP_ENTITY_TABLE set $STARTS_AMOUNT = $STARTS_AMOUNT +  1 where $PACKAGE_NAME = :appPackage")
    fun incAppStarts(appPackage: String)

    @Query("UPDATE $APP_ENTITY_TABLE set $STARTS_AMOUNT = :amountStarts where $PACKAGE_NAME = :appPackage")
    fun updateAppStarts(appPackage: String, amountStarts: Int): Int

    @Query("SELECT * from $APP_ENTITY_TABLE where $LAST_USE > 0 order by $LAST_USE DESC")
    fun getUsedToCursor(): Cursor

    @Query("SELECT * from $APP_ENTITY_TABLE where $LAST_USE order by $LAST_USE DESC LIMIT 1")
    fun getLastUsedToCursor(): Cursor
}
package com.example.anischik.database

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.anischik.models.AppEntity
import com.example.anischik.models.FeaturedEntity
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class AppRepository(applicationContext: Context) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var appDao: AppDao
    private var apps: LiveData<List<AppEntity>>
    private var scope = CoroutineScope(Dispatchers.Default)

    init {
        val db = MyAppDatabase.getDatabase(
            applicationContext
        )
        appDao = db.appDao()
        apps = appDao.getApps()
    }

    fun getApps(): LiveData<List<AppEntity>> = apps

    fun getFeaturedApps() = appDao.getFeaturedApps()

    fun setFeatured(featuredApp: FeaturedEntity) = scope.async {
        appDao.setFeatured(featuredApp)
    }

    fun removeFromFeatured(position: Int) = scope.launch {
        appDao.updateFeatured(null, null, null, null, position)
    }

    fun removeFromFeatured(label: String, uri: String) = scope.launch { appDao.removeFromFeatured(label, uri) }

    fun insertApp(app: AppEntity?) = scope.launch(Dispatchers.Default) {
                appDao.insertApp(app)
    }

    fun getApp(appPackage: String): AppEntity?  = appDao.getEntity(appPackage)

    fun deleteApp(app: AppEntity?) = scope.launch(Dispatchers.Default)  {  appDao.deleteApp(app) }

    fun deleteApp(appPackage: String) = scope.launch(Dispatchers.Default) { appDao.deleteApp(appPackage) }

    fun setUsedAppTime(appPackage: String, time: Long) = scope.launch { appDao.setUsed(appPackage, time) }

    fun getLastUsedApp() = appDao.getUsed(1)

    fun incAppStarts(packageName: String) = scope.launch { appDao.incAppStarts(packageName) }

    fun updateFromList(entityList: List<AppEntity>) = scope.launch { appDao.updateFromList(entityList) }

    fun clearBase() = scope.launch { appDao.deleteAll() }
}
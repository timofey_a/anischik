package com.example.anischik.database

import android.content.Context
import androidx.room.*
import com.example.anischik.models.AppEntity
import com.example.anischik.models.FeaturedEntity
import com.example.anischik.models.ProfileEntity

@Database(entities = [AppEntity::class, ProfileEntity::class, FeaturedEntity::class], version = 1, exportSchema = false)
abstract class MyAppDatabase: RoomDatabase() {

    abstract fun appDao() : AppDao
    abstract fun profileDao(): ProfileDao


    companion object {

        @Volatile
        private var INSTANCE: MyAppDatabase? = null

        private const val DB_NAME = "apps_db"

        fun getDatabase(context: Context): MyAppDatabase {
            if (INSTANCE == null){
                synchronized(this){
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        MyAppDatabase::class.java, DB_NAME
                    )
                        .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}
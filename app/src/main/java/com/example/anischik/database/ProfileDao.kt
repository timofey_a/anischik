package com.example.anischik.database

import android.database.Cursor
import androidx.room.*
import com.example.anischik.models.ProfileEntity
import com.example.anischik.models.ProfileEntity.Companion.ID
import com.example.anischik.models.ProfileEntity.Companion.PROFILE_TABLE

@Dao
interface ProfileDao {

    @Query("SELECT * from $PROFILE_TABLE where $ID = 1")
    fun getProfile(): ProfileEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateProfile(entity: ProfileEntity?)

    @Query("SELECT * from $PROFILE_TABLE where $ID = 1")
    fun getProfileToCursor(): Cursor
}
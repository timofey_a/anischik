package com.example.anischik.interfaces

import com.example.anischik.adapter.AppsAdapter

interface AppStorage {
    fun getAppsAdapter(): AppsAdapter
}
package com.example.anischik.interfaces

import androidx.navigation.NavController

interface WithNavController {
    fun getNavController(): NavController
}
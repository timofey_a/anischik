package com.example.anischik.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.anischik.models.AppEntity.Companion.APP_ENTITY_TABLE

@Entity(tableName = APP_ENTITY_TABLE)
data class AppEntity(

    @PrimaryKey
    @ColumnInfo(name = PACKAGE_NAME) val packageName: String,

    @ColumnInfo(name = LABEL) var label: String
) {

    @ColumnInfo(name = STARTS_AMOUNT) var amount: Int = 0

    @ColumnInfo(name = FAVOURITE) var favourite: Int = 0

    @ColumnInfo(name = LAST_USE) var lastUse: Long = 0

    companion object {
        const val APP_ENTITY_TABLE = "application_table"

        const val PACKAGE_NAME = "packageName"
        const val LABEL = "appLabel"
        const val STARTS_AMOUNT = "amount"
        const val FAVOURITE = "favourite"
        const val LAST_USE = "lastUse"
    }
}
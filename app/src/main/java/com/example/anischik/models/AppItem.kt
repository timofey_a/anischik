package com.example.anischik.models

import android.graphics.drawable.Drawable

data class AppItem(
    var appPackage: String,
    var label: String,
    var icon: Drawable?,
    var installTime: Long = 0,
    var updateTime: Long = 0,
    var launchAmount: Int = 0,
    var lastStart: Long = 0
)
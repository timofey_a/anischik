package com.example.anischik.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.anischik.models.FeaturedEntity.Companion.FEATURED_APPS_TABLE

@Entity(tableName = FEATURED_APPS_TABLE)
data class FeaturedEntity(

    @ColumnInfo(name = LABEL) var label: String?,

    @ColumnInfo(name = INTENT) var intent: String?,

    @ColumnInfo(name = URI) var uri: String?,

    @ColumnInfo(name = PACKAGE_NAME) var packagename: String? = null,

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = POSITION) var position: Int? = null
    ) {

    companion object {
        const val FEATURED_APPS_TABLE = "featured_apps_table"

        const val LABEL = "label"
        const val PACKAGE_NAME = "packageName"
        const val ADDRESS = ""
        const val INTENT = "intent"
        const val URI = "uri"
        const val POSITION = "position"
    }
}
package com.example.anischik.models

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.anischik.R
import com.example.anischik.models.ProfileEntity.Companion.PROFILE_TABLE

@Entity(tableName = PROFILE_TABLE)
data class ProfileEntity(

    @ColumnInfo(name = PHONE_NUMBER) var phoneNumber: String,

    @ColumnInfo(name = TELEGRAM) var telegram: String,

    @ColumnInfo(name = VK) var vk: String,

    @ColumnInfo(name = MAIL) var mail: String,

    @ColumnInfo(name = GITLAB) var gitlab: String,

    @ColumnInfo(name = LOCATION) var location: String,

    @PrimaryKey
    @ColumnInfo(name = ID) val id:Int = 1
) {
    companion object {
        const val PROFILE_TABLE = "profile_table"

        const val ID = "id"
        const val PHONE_NUMBER = "phoneNumber"
        const val TELEGRAM = "telegram"
        const val VK = "vk"
        const val MAIL = "mail"
        const val GITLAB = "gitlab"
        const val LOCATION = "location"

        fun getDefault(context: Context): ProfileEntity {
            return ProfileEntity(
                phoneNumber = context.getString(R.string.my_phone_number),
                telegram = context.getString(R.string.my_telegram_name),
                vk = context.getString(R.string.my_vk_name),
                mail = context.getString(R.string.my_mail),
                gitlab = context.getString(R.string.my_gitlab_name),
                location = context.getString(R.string.my_location)
            )
        }
    }
}
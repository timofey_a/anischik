package com.example.anischik.provider

import android.content.ContentProvider
import android.content.ContentResolver
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log
import com.example.anischik.models.AppEntity.Companion.PACKAGE_NAME
import com.example.anischik.models.AppEntity.Companion.STARTS_AMOUNT
import com.example.anischik.database.MyAppDatabase
import com.yandex.metrica.YandexMetrica
import java.lang.Exception

class AppContentProvider : ContentProvider() {


    private lateinit var appDB: MyAppDatabase

    companion object {
        const val AUTHORITY = "com.example.anischik.provider"

        private const val APPS = 1
        private const val LAST_APP = 2
        private const val USER_INFO = 3

        private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            uriMatcher.addURI(AUTHORITY, "apps", APPS)
            uriMatcher.addURI(AUTHORITY, "app_last", LAST_APP)
            uriMatcher.addURI(AUTHORITY, "user_info", USER_INFO)
        }
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? = null

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        YandexMetrica.reportEvent("content provider: method 'query' called")
        return when (uriMatcher.match(uri)) {
            APPS -> appDB.appDao().getUsedToCursor()
            LAST_APP -> appDB.appDao().getLastUsedToCursor()
            USER_INFO -> appDB.profileDao().getProfileToCursor()
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }

    override fun onCreate(): Boolean {
        try {
            appDB = MyAppDatabase.getDatabase(context!!)
            return true
        } catch (_: Exception) {
            throw Exception("Can't create database: context is null")
        }
    }

    override fun update(
        uri: Uri,
        values: ContentValues,
        selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        YandexMetrica.reportEvent("content provider: method 'update' called")
        val apps = appDB.appDao()

        when (uriMatcher.match(uri)) {
            APPS -> {
                val appPackage = values.getAsString(PACKAGE_NAME)
                val amount = values.getAsInteger(STARTS_AMOUNT)
                return apps.updateAppStarts(appPackage, amount)
            }
            else -> throw java.lang.IllegalArgumentException("Unsupported URI: $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int = 0

    override fun getType(uri: Uri): String? {
        YandexMetrica.reportEvent("content provider: method 'getType' called")
        return when (uriMatcher.match(uri)) {
            APPS -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_apps"
            LAST_APP -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_last_app"
            USER_INFO -> ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.$AUTHORITY.provider_user_info"
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }

}
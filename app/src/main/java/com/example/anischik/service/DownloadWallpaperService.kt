package com.example.anischik.service

import android.app.IntentService
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import com.example.anischik.uiComponents.Pref
import com.example.anischik.utils.wallpaper.WallpaperUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DownloadWallpaperService : IntentService("DownloadWallpaperService") {

    companion object {
        const val ACTION_DOWNLOAD_WALLPAPERS =
            "com.example.anischik.action.DOWNLOAD_WALLPAPERS"
        const val BROADCAST_ACTION_UPDATE_WALLPAPERS =
            "com.example.anischik.action.UPDATE_WALLPAPERS"
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.d(this::class.java.simpleName.toString(), "onHandleIntent called")
        when (intent?.action) {
            ACTION_DOWNLOAD_WALLPAPERS -> {
                try {
                    val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                    val source = prefs.getString(Pref.WALLPAPER_SOURCE, Pref.WallpaperSource.ONE)
                    CoroutineScope(Dispatchers.Default).launch {
                        WallpaperUtils.updateWallpaper(source).join()

                        val wallpaperIntent = Intent()
                        wallpaperIntent.action = BROADCAST_ACTION_UPDATE_WALLPAPERS
                        sendBroadcast(wallpaperIntent)
                    }
                } catch (_: Exception) {
                    Log.e(this::class.simpleName, "Can't update wallpaper")
                }
            }
        }
    }
}

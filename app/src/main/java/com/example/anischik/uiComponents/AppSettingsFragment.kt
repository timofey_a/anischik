package com.example.anischik.uiComponents

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.*
import com.example.anischik.R
import com.example.anischik.utils.AppStorage
import com.yandex.metrica.YandexMetrica

class AppSettingsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    var needUpdate = false
    var firstStart = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val settingView = super.onCreateView(inflater, container, savedInstanceState)
        firstStart = savedInstanceState == null
        return settingView
    }

    override fun onStart() {
        super.onStart()
        if (firstStart) {
            YandexMetrica.reportEvent("settings fragment launched")
        }
    }

    private fun onChangeSort(key: String, newValue: Any): Boolean {
        when(key) {
            Pref.APPS_SORTING -> (activity as? AppStorage)?.getAppsAdapter()?.reSort(newValue as? String) != null
            Pref.REVERSE_SORTING -> (activity as? AppStorage)?.getAppsAdapter()?.reSort(null, newValue as? Boolean)
        }
        return true
    }

    private fun changeSettings(key: String, newValue: Any): Boolean {
        YandexMetrica.reportEvent("preference '${key}' changed", newValue.toString())
        return true
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(prefs: SharedPreferences?, key: String?) {
        if (prefs == null) {
            return
        }
        var value: Any? = null
        when (key) {
            Pref.DARK_MODE -> {
                value = prefs.getBoolean(key, false)
                changeSettings(key, value)
                AppCompatDelegate.setDefaultNightMode(
                    if (value)
                        AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
                )
                return
            }
            Pref.REVERSE_SORTING -> {
                value = prefs.getBoolean(key, false)
                onChangeSort(key, value)
            }
            Pref.APPS_SORTING -> {
                value = prefs.getString(key, Pref.AppsSorting.NO_SORT)
                onChangeSort(key, value)
            }
            Pref.FIRST_START -> {
                value = prefs.getBoolean(key, false)
            }
            Pref.TIGHT_LAYOUT -> {
                value = prefs.getBoolean(key, false)
            }
        }
        if ((key != null) and (value != null)) {
            changeSettings(key!!, value!!)
        }
    }
}



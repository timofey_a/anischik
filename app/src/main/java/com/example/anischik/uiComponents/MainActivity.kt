package com.example.anischik.uiComponents

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.anischik.R
import com.example.anischik.database.MyAppDatabase
import com.example.anischik.models.ProfileEntity
import com.example.anischik.uiComponents.customs.PersonInfoItem
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.my_profile_menu.*
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {

    private lateinit var myProfile: ProfileEntity
    private lateinit var db: MyAppDatabase
    var profileChanged = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        fillProfile()

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("Profile activity launched")
            val toast = Toast.makeText(this, getString(R.string.start_toast), Toast.LENGTH_LONG)
            toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.show()
        }

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val pushValue = prefs.getString(Pref.SILENT_PUSH_VALUE, "")
        if (pushValue != "") {
            silent_push_value.visibility = View.VISIBLE
            silent_push_value.textTitle = pushValue
        }

        setPersonInfoClickListeners()
    }

    private fun setProfile(newProfile: ProfileEntity) {
        myProfile.let {
            phone_number.textTitle = myProfile.phoneNumber
            telegram_name.textTitle = myProfile.telegram
            vk_name.textTitle = myProfile.vk
            mail_address.textTitle = myProfile.mail
            gitlab_name.textTitle = myProfile.gitlab
            location.textTitle = myProfile.location
        }
    }

    private fun fillProfile() {
        CoroutineScope(Dispatchers.Default).launch {
            db = MyAppDatabase.getDatabase(this@MainActivity)
            val temp = db.profileDao().getProfile()

            if (temp == null) {
                myProfile = ProfileEntity.getDefault(this@MainActivity)
                suspend {
                    db.profileDao().updateProfile(myProfile)
                }
            } else {
                myProfile = temp
            }

            withContext(Dispatchers.Main) {
                setProfile(myProfile)
            }
        }
    }

    private fun reportEvent(field: String, value: String) {
        YandexMetrica.reportEvent("transition on $field", value)
    }

    private fun setPersonInfoClickListeners() {
        phone_number?.setOnClickListener {
            reportEvent("phone number", myProfile.phoneNumber)
            val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${phone_number.textTitle}"))
            startActivity(callIntent)
        }
        telegram_name?.setOnClickListener {
            reportEvent("telegram", myProfile.telegram)
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://t.me/${telegram_name.textTitle.getName()}")
            )
            startActivity(webIntent)
        }
        vk_name?.setOnClickListener {
            reportEvent("vk", myProfile.vk)
            val webIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.me/${vk_name.textTitle.getName()}"))
            startActivity(webIntent)
        }
        mail_address?.setOnClickListener {
            reportEvent("mail", myProfile.mail)
            val mailIntent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, arrayOf(mail_address.textTitle))
            }
            startActivity(mailIntent)
        }
        gitlab_name?.setOnClickListener {
            reportEvent("gitlab", myProfile.gitlab)
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://gitlab.com/${gitlab_name.textTitle.getName()}")
            )
            startActivity(webIntent)
        }
        location?.setOnClickListener {
            reportEvent("location", myProfile.location)
            val geoIntent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:${location.textTitle}"))
            startActivity(geoIntent)
        }

        for(item in listOf<PersonInfoItem>(phone_number, telegram_name, vk_name, mail_address, gitlab_name, location))
            item.setOnLongClickListener { onLongClick(item); true}
    }

    private fun onLongClick(p: PersonInfoItem) {
        val builder = AlertDialog.Builder(this)
        val frame = FrameLayout(this)
        val input = EditText(this)
        val padding: Int = resources.getDimension(R.dimen.dialog_padding).toInt()
        input.hint = getString(R.string.new_value)
        frame.addView(input)
        frame.setPadding(padding * 2, padding, padding * 2, padding)

        builder.apply {
            setTitle(p.textSecondary)
            setView(frame)
            setPositiveButton(getString(R.string.save)) { _, _ ->
                if(input.text.toString() != "") {
                    p.textTitle = input.text.toString()
                    updateProfileEntity()
                    GlobalScope.launch {
                        db.profileDao().updateProfile(myProfile)
                    }
                }
                else {
                    Toast.makeText(this@MainActivity, context?.getString(R.string.empty_input), Toast.LENGTH_LONG).show()
                }
            }
            setNegativeButton(context.getString(R.string.cancel)) { _, _ -> Unit }
            show()
        }
    }

    private fun updateProfileEntity() {
        profileChanged = true
        myProfile.apply {
            phoneNumber = phone_number.textTitle ?: ""
            telegram = telegram_name.textTitle ?: ""
            vk = vk_name.textTitle ?: ""
            mail = mail_address.textTitle ?: ""
            gitlab = gitlab_name.textTitle ?: ""
            location = this@MainActivity.location.textTitle ?: ""
        }
    }

    private fun String?.getName() = this?.let { substring(1) }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {

        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onPause() {
             val eventParams = mapOf(
            "phone number" to myProfile.phoneNumber,
            "telegram" to myProfile.telegram,
            "vk" to myProfile.vk,
            "mail" to myProfile.mail,
            "gitlab" to myProfile.gitlab,
            "location" to myProfile.location
        )
        YandexMetrica.reportEvent("profile info changed", eventParams)
        super.onPause()
    }
}

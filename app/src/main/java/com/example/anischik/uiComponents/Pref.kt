package com.example.anischik.uiComponents

abstract class Pref {
    companion object {
        const val DARK_MODE = "darkMode"
        const val TIGHT_LAYOUT = "tightLayout"
        const val FIRST_START = "firstStart"

        const val APPS_SORTING = "appsSorting"
        const val REVERSE_SORTING = "reverseSort"

        const val WALLPAPER_SOURCE = "wallpaperSource"
        const val WALLPAPER_UPDATE_FREQUENCY = "wallpaperUpdateFrequency"

        const val SILENT_PUSH_VALUE = "silentPushValue"
    }

    abstract class AppsSorting {
        companion object {
            const val NO_SORT = "no sort"
            const val BY_LABEL = "by label"
            const val BY_LAUNCH_AMOUNT = "by popularity"
            const val BY_INSTALL_TIME = "by install time"
            const val BY_UPDATE_TIME = "by update time"
        }
    }

    abstract class WallpaperSource {
        companion object {
            const val ONE = "https://picsum.photos/1000/2000"
            const val TWO = "https://loremflickr.com/1000/2000"
            const val THREE = "https://placeimg.com/1000/2000/any"
            const val FOUR = "https://source.unsplash.com/random/1000*2000"
            const val FIVE = "https://www.random.org/bitmaps/?format=png&width=300&height=300&zoom=1"
        }
    }

    abstract class WallpaperUpdateFrequency {
        companion object {
            const val ONE = "15"
            const val TWO = "60"
            const val THREE = "480"
            const val FOUR = "1440"
        }
    }
}


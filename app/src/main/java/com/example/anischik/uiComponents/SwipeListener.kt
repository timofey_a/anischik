package com.example.anischik.uiComponents

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.anischik.MyApplication
import com.example.anischik.R
import kotlin.math.abs
import kotlin.math.max

abstract class SwipeListener(val context: Context): GestureDetector.SimpleOnGestureListener() {

    abstract fun onSwipeUp(): Boolean
    abstract fun onSwipeDown(): Boolean
    abstract fun onSwipeRight(): Boolean
    abstract fun onSwipeLeft():Boolean

    companion object {
        private val SWIPE_MIN_DISTANCE = MyApplication.appContext.resources.getDimension(R.dimen.home_swipe_distance)
        private const val SWIPE_THRESHOLD_VELOCITY = 400
    }
    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        if (e1 == null || e2 == null)
            return false

        return if(e1.y - e2.y > SWIPE_MIN_DISTANCE && abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
            onSwipeUp() // снизу вверх
        } else if (e2.y - e1.y > SWIPE_MIN_DISTANCE && abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
            onSwipeDown()
        } else if (e2.x - e1.x > SWIPE_MIN_DISTANCE && abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            onSwipeRight()
        } else if (e1.x - e2.x > SWIPE_MIN_DISTANCE && abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            onSwipeLeft()
        } else {
            false
        }
    }
}
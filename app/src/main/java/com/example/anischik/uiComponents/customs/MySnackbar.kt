package com.example.anischik.uiComponents.customs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.anischik.R
import com.example.anischik.utils.SnackbarCallbackLog
import com.google.android.material.snackbar.Snackbar

class MySnackbar(
    private var parentView: View,
    var text: String,
    var duration: Int,
    var toTopOf: View? = null
) {

    val snackbar: Snackbar
    val layout: CoordinatorLayout
    private val viewGroupParent: ViewGroup

    init {
        viewGroupParent = findViewGroup(parentView)
        layout = viewGroupParent.findViewById(R.id.snackbar_layout)
            ?: (LayoutInflater.from(parentView.context)
                .inflate(R.layout.my_snackbar_layout, findViewGroup(parentView), true)
                .findViewById(R.id.snackbar_layout) as CoordinatorLayout)
    }

    init {
        snackbar = Snackbar.make(layout, text, duration)

        snackbar
            .addCallback(SnackbarCallbackLog())
            .apply {
                view.background = parentView.context.getDrawable(R.drawable.draw_snackbar)
            }.animationMode = Snackbar.ANIMATION_MODE_FADE

        toTopOf?.let {
            val parentTop = (it.parent as ViewGroup).height - it.top

            val params = layout.layoutParams as? CoordinatorLayout.LayoutParams
            params?.setMargins(params.leftMargin, params.topMargin, params.rightMargin, parentTop)
            layout.layoutParams = params
        }
    }

    fun setAction(action: String, actionFun: (View) -> Unit) =
        apply { snackbar.setAction(action, actionFun) }

    fun show() {
        snackbar.show()
    }

    private fun findViewGroup(view: View): ViewGroup {
        var current: View = view
        var next: View? = view
        while (next != null) {

            if ((next as? ViewGroup) != null) {
                if (next is CoordinatorLayout)
                    return next
                current = next
            }

            next = next.parent as? View
        }

        return current as ViewGroup
    }

}
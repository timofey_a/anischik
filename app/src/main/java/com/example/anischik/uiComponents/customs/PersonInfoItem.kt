package com.example.anischik.uiComponents.customs

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.example.anischik.R
import kotlinx.android.synthetic.main.person_info_item.view.*

class PersonInfoItem(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    constructor(context: Context, textTitle: String, imageMain: Int? = null, textSecond: String? = null, imageSecond: Int? = null) : this(context, null) {
        this.textTitle = textTitle
        this.imageMain = imageMain
        this.textSecondary = textSecond
        this.imageAdd = imageSecond
        initView(view)
    }

    val view: View = LayoutInflater.from(context).inflate(R.layout.person_info_item, this)

    private var imageMain: Int? = null
    set(value) {
        field = value
        view.image_main.setImageDrawable(context.getDrawable(value ?: R.drawable.empty))
    }
    private var imageAdd: Int? = null
    set(value) {
        field = value
        view.image_additional.setImageDrawable(context.getDrawable(imageAdd ?: R.drawable.empty))
    }
    var textTitle: String? = null
    set(value) {
        field = value
        view.text_title.text = textTitle ?: ""
    }
    var textSecondary: String? = null
    set(value) {
        field = value
        view.text_secondary.text = textSecondary ?: ""
    }

    init {
        if(attrs != null)
            context.theme.obtainStyledAttributes(attrs, R.styleable.PersonInfoItem, 0, 0).apply {
                textTitle = getString(R.styleable.PersonInfoItem_textName)
                textSecondary = getString(R.styleable.PersonInfoItem_textSecond)
                imageMain = getResourceId(R.styleable.PersonInfoItem_iconMain, R.drawable.empty)
                imageAdd = getResourceId(R.styleable.PersonInfoItem_iconSecondary, R.drawable.empty)
                initView(view)
            }
    }

    private fun initView(view: View) {
        view.text_title.text = textTitle ?: ""
        view.text_secondary.text = textSecondary ?: ""
        view.image_main.setImageDrawable(context.getDrawable(imageMain ?: R.drawable.empty))
        view.image_additional.setImageDrawable(context.getDrawable(imageAdd ?: R.drawable.empty))
    }

    fun setItemsClickListener(onClickListener: (View) -> Unit) {
        view.text_title.setOnClickListener(onClickListener)
        view.text_secondary.setOnClickListener(onClickListener)
        view.image_main.setOnClickListener(onClickListener)
        view.image_additional.setOnClickListener(onClickListener)
    }
}
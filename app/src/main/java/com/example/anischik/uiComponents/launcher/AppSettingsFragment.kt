package com.example.anischik.uiComponents.launcher

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.preference.*
import com.example.anischik.MyApplication
import com.example.anischik.R
import com.example.anischik.WithBackground
import com.example.anischik.interfaces.AppStorage
import com.example.anischik.service.DownloadWallpaperService
import com.example.anischik.uiComponents.Pref
import com.example.anischik.uiComponents.customs.MySnackbar
import com.example.anischik.utils.wallpaper.WallpaperUtils
import com.yandex.metrica.YandexMetrica

class AppSettingsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    var firstStart = true
    private var settingView: View? = null
    override fun getView() = settingView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        (activity as? WithBackground)?.wallpaper?.isVisible = false

        settingView = super.onCreateView(inflater, container, savedInstanceState)
        firstStart = savedInstanceState == null
        preferenceScreen.findPreference<Preference>("updateWallpaperNow")
            ?.onPreferenceClickListener =
           Preference.OnPreferenceClickListener {
               if (settingView != null) {
                   val intent = Intent(MyApplication.appContext, DownloadWallpaperService::class.java)
                       .setAction(DownloadWallpaperService.ACTION_DOWNLOAD_WALLPAPERS)
                   settingView?.context?.startService(intent)
                   MySnackbar(settingView!!, settingView!!.context.getString(R.string.wallpaper_updated), 1500).show()
               } else {
                   MySnackbar(settingView!!, settingView!!.context.getString(R.string.wallpaper_updated), 1500).show()
               }
               true
           }

        return settingView
    }

    override fun onStop() {
        (activity as? WithBackground)?.wallpaper?.isVisible = true
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        if (firstStart) {
            YandexMetrica.reportEvent("settings fragment launched")
        }
    }

    private fun onChangeSort(key: String, newValue: Any): Boolean {
        when(key) {
            Pref.APPS_SORTING -> (activity as? AppStorage)?.getAppsAdapter()?.reSort(newValue as? String) != null
            Pref.REVERSE_SORTING -> (activity as? AppStorage)?.getAppsAdapter()?.reSort(null, newValue as? Boolean)
        }
        return true
    }

    private fun changeSettings(key: String, newValue: Any): Boolean {
        Log.d(this::class.simpleName, "preference '${key}' changed\",")
        YandexMetrica.reportEvent("preference '${key}' changed", newValue.toString())
        return true
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(prefs: SharedPreferences?, key: String?) {
        if (prefs == null) {
            return
        }
        var value: Any? = null
        when (key) {
            Pref.DARK_MODE -> {
                value = prefs.getBoolean(key, false)
                changeSettings(key, value)
                AppCompatDelegate.setDefaultNightMode(
                    if (value)
                        AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO
                )
                return
            }
            Pref.REVERSE_SORTING -> {
                value = prefs.getBoolean(key, false)
                onChangeSort(key, value)
            }
            Pref.APPS_SORTING -> {
                value = prefs.getString(key,
                    Pref.AppsSorting.NO_SORT
                )
                onChangeSort(key, value)
            }
            Pref.FIRST_START -> {
                value = prefs.getBoolean(key, false)
            }
            Pref.TIGHT_LAYOUT -> {
                value = prefs.getBoolean(key, false)
            }
            Pref.WALLPAPER_SOURCE -> {
                value = prefs.getString(key, Pref.WallpaperSource.ONE)
                val intent = Intent(MyApplication.appContext, DownloadWallpaperService::class.java)
                    .setAction(DownloadWallpaperService.ACTION_DOWNLOAD_WALLPAPERS)
                settingView?.context?.startService(intent)
            }
            Pref.WALLPAPER_UPDATE_FREQUENCY -> {
                value = prefs.getString(key, Pref.WallpaperUpdateFrequency.ONE)
                WallpaperUtils.addWallpaperWork((value as String).toInt())
            }
        }
        if ((key != null) and (value != null)) {
            changeSettings(key!!, value!!)
        }
    }
}



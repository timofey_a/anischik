package com.example.anischik.uiComponents.launcher

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.R
import com.example.anischik.database.AppRepository
import com.example.anischik.interfaces.WithNavController
import com.example.anischik.models.FeaturedEntity
import com.example.anischik.uiComponents.customs.MySnackbar
import com.google.android.material.appbar.AppBarLayout
import com.squareup.picasso.Picasso
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.item_app_grid.view.*
import kotlinx.android.synthetic.main.launcher_home.*
import kotlinx.android.synthetic.main.launcher_home.view.*
import kotlinx.coroutines.*
import java.util.*

class HomeFragment : Fragment() {

    lateinit var myView: View
    override fun getView() = myView

    private lateinit var recyclerView: RecyclerView
    private lateinit var appRep: AppRepository
    private val pm: PackageManager
        get() = myView.context.packageManager

    companion object {
        const val INTENT_PICK_CONTACT = 1
        private lateinit var featuredAdapter: MyAdapter
        private var clickPosition: Int? = null
    }

    var toSettings = false

    inner class MyAdapter : RecyclerView.Adapter<FeaturedViewHolder>() {

        var list = mutableListOf<FeaturedItem>()

        var onItemClick: (View?, Int) -> Unit = { _, _ -> }
        var onItemLongClick: (View?, Int) -> Boolean = { _, _ -> false }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_app_grid, parent, false)
            return FeaturedViewHolder(view)
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
            holder.bind(list[position], onItemClick, onItemLongClick)
        }
    }

    inner class FeaturedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var application: FeaturedItem? = null

        fun bind(
            appItem: FeaturedItem,
            onItemClick: ((View?, Int) -> Unit) = { _, _ -> },
            onItemLongClick: ((View?, Int) -> Boolean) = { _, _ -> false }
        ) {
            application = appItem
            if (appItem.label == null) {
                itemView.card_view.visibility = View.INVISIBLE
            } else if (appItem.icon != null) {
                itemView.card_view.visibility = View.VISIBLE
                itemView.icon.setImageDrawable(appItem.icon)
            } else if (appItem.isContact()) {
                itemView.card_view.visibility = View.VISIBLE
                itemView.icon.setImageResource(R.drawable.baseline_person_contact)
            } else {
                itemView.card_view.visibility = View.VISIBLE
                Picasso.get()
                    .load("https://favicon.yandex.net/favicon/${appItem.uri?.removeHttp()}?size=32")
                    .into(itemView.icon)
            }
            itemView.text_view.text = appItem.label
            itemView.apply {
                setOnLongClickListener {
                    if (text_view.text == "") onItemLongClick(it, layoutPosition) else true
                }
                setOnClickListener {
                    if (text_view.text == "") onItemClick(it, layoutPosition)
                }
                card_view.setOnClickListener { onItemClick(it, layoutPosition) }
                text_view.setOnClickListener { onItemClick(it, layoutPosition) }
                card_view.setOnLongClickListener { onItemLongClick(it, layoutPosition) }
                text_view.setOnLongClickListener { onItemLongClick(it, layoutPosition) }
            }
        }
    }

    data class FeaturedItem(
        var label: String?,
        var intent: String?,
        var uri: String?,
        var appPackage: String?,
        var icon: Drawable?
    ) {
        fun isContact() = intent == Intent.ACTION_DIAL
        fun isSite() = intent == Intent.ACTION_VIEW
        fun isApp() = intent == null && appPackage != null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflater.inflate(R.layout.launcher_home, container, false)

        appRep = AppRepository(requireActivity().applicationContext)

        makeRecyclerView()
        featuredAdapter.onItemClick = ::onItemClick
        featuredAdapter.onItemLongClick = ::onItemLongClick

        val buttonScope = CoroutineScope(Dispatchers.Default)

        button_all_apps.setOnClickListener {
            if (!toSettings) {
                (activity as WithNavController).getNavController()
                    .navigate(R.id.action_nav_home_to_nav_launcher)
            } else {
                buttonScope.cancel()
                button_all_apps.setImageResource(R.drawable.baseline_view_module_24)
                toSettings = false
                (activity as WithNavController).getNavController()
                    .navigate(R.id.action_nav_home_to_nav_settings)
            }
        }

        button_all_apps.setOnLongClickListener() {
            button_all_apps.setImageResource(R.drawable.baseline_settings_24)
            toSettings = true
            buttonScope.launch {
                delay(2000)

                withContext(Dispatchers.Main) {
                    toSettings = false
                    button_all_apps.setImageResource(R.drawable.baseline_view_module_24)
                }
            }
            true
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        //val dv = activity?.findViewById<DrawerLayout>(R.id.drawer_layout)
        //dv?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
        val layout = activity?.findViewById<AppBarLayout>(R.id.toolbar_layout)
        layout?.isVisible = false
    }

    override fun onResume() {
        super.onResume()
        fillFeaturedList()
    }

    override fun onStop() {
        val dv = activity?.findViewById<DrawerLayout>(R.id.drawer_layout)
        dv?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED)
        (activity as? AppCompatActivity)?.supportActionBar?.show()
        val layout = activity?.findViewById<AppBarLayout>(R.id.toolbar_layout)
        layout?.isVisible = true
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            INTENT_PICK_CONTACT -> {
                if (resultCode == Activity.RESULT_OK) {
                    val contactData = data?.data
                    if (contactData != null) {
                        val c = view.context.contentResolver
                            ?.query(contactData, null, null, null, null)
                        if (c?.moveToFirst() == true) {
                            val name =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                            val number: String?
                            val contactId: String =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))
                            if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                                val phones = view.context.contentResolver
                                    .query(
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                        null, null
                                    )
                                if (phones?.moveToNext() == true) {
                                    number =
                                        phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                    Log.d("1111", "position: $clickPosition")

                                    appRep.setFeatured(
                                        FeaturedEntity(
                                            label = name,
                                            uri = "tel:${number}",
                                            intent = Intent.ACTION_DIAL,
                                            position = clickPosition
                                        )
                                    )
                                }
                                phones?.close()
                            }
                        }
                        c?.close()
                        clickPosition = null
                    }
                }
            }
        }
    }

    private fun onItemClick(view: View?, position: Int) {
        val pressed = featuredAdapter.list[position]

        if ((pressed.intent == null) and (pressed.uri == null))
            return

        val intent = if (pressed.intent == null) {
            Intent.parseUri(pressed.uri, 0)
        } else {
            Intent(pressed.intent, Uri.parse(pressed.uri))
        }

        pressed.appPackage?.let {
            appRep.incAppStarts(it)
        }
        Log.d("1111", "click uri: ${pressed.uri}")

        startActivity(intent)
    }

    private fun onItemLongClick(view: View?, position: Int): Boolean {
        val popupMenu = PopupMenu(myView.context, view)
        val cur = featuredAdapter.list[position]
        val popupId =
            if (cur.label != null) R.menu.featured_popup_menu else R.menu.featured_null_popup_menu
        popupMenu.menuInflater.inflate(popupId, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.featured_popup_info -> {
                    if (cur.isApp()) {
                        val packageUri = Uri.parse("package:${cur.appPackage}")
                        val intent =
                            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageUri)
                        YandexMetrica.reportEvent(
                            "app info opened",
                            featuredAdapter.list[position].label
                        )
                        startActivity(intent)
                    } else {
                        val description = if (cur.isContact()) myView.context.getString(R.string.phone_number)
                            else myView.context.getString(R.string.description_link)
                        val text = if (cur.isContact()) cur.uri?.substring(4) else cur.uri

                        MySnackbar(myView, "$description:\n$text", 3000, myView.button_all_apps)
                            .setAction(myView.context.getString(R.string.copy)) {
                                val clipData = ClipData.newPlainText(description, text)
                                (context?.getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager)
                                    ?.primaryClip = clipData

                                val copied = myView.context.getString(R.string.text_copied)
                                Toast.makeText(myView.context, "$description $copied", Toast.LENGTH_SHORT).show()

                            }.show()

                    }
                }
                R.id.featured_popup_move -> {
                    val curListener = featuredAdapter.onItemClick
                    featuredAdapter.onItemClick = { _, pos ->
                        val entity1 = cur.toFeaturedEntity(pos)
                        val entity2 = featuredAdapter.list[pos].toFeaturedEntity(position)
                        appRep.setFeatured(entity1)
                        appRep.setFeatured(entity2)
                        Collections.swap(featuredAdapter.list, position, pos)
                        featuredAdapter.notifyDataSetChanged()
                        featuredAdapter.onItemClick = curListener
                    }
                    featuredAdapter.notifyDataSetChanged()
                    Toast.makeText(
                        myView.context,
                        getString(R.string.move_item_description),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                R.id.featured_popup_remove -> {
                    appRep.removeFromFeatured(position)
                }
                R.id.featured_add_link -> {
                    addLinkDialog(position)
                }
                R.id.featured_add_contact -> {
                    addContactRequest(position)
                }
            }
            true
        }
        popupMenu.show()
        return true
    }

    private fun fillFeaturedList() {
        appRep.getFeaturedApps()
            .observe(requireActivity(), androidx.lifecycle.Observer { list ->
                featuredAdapter.list =
                    list
                        .map { it.toFeaturedItem() }
                        .toMutableList()
                featuredAdapter.notifyDataSetChanged()
            })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun makeRecyclerView() {

        featuredAdapter = MyAdapter()


        recyclerView = view.recycler_view
        recyclerView.adapter = featuredAdapter
        recyclerView.layoutManager = object : GridLayoutManager(view.context, 5) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        recyclerView.isScrollContainer = false
        recyclerView.isNestedScrollingEnabled = false
    }

    private fun addContactRequest(position: Int) {
        clickPosition = position

        checkContactsPermission()
        if (ContextCompat.checkSelfPermission(
                view.context,
                android.Manifest.permission.READ_CONTACTS
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
            startActivityForResult(intent, INTENT_PICK_CONTACT)
        }
    }

    private fun addLinkDialog(position: Int) {
        val builder = AlertDialog.Builder(myView.context)
        val frame = LinearLayout(myView.context)
        frame.orientation = LinearLayout.VERTICAL
        val inputLink = EditText(myView.context)
        val inputLabel = EditText(myView.context)
        val padding: Int = resources.getDimension(R.dimen.dialog_padding).toInt()
        inputLink.hint = getString(R.string.description_link)
        inputLabel.hint = getString(R.string.description_name)
        frame.addView(inputLink)
        frame.addView(inputLabel)
        frame.setPadding(padding * 2, padding, padding * 2, padding)

        builder.apply {
            setTitle(view.context.getString(R.string.add_site_link))
            setView(frame)
            setPositiveButton("ADD") { _, _ ->
                var link = inputLink.text.toString()
                var name = inputLabel.text.toString()
                name = if (name == "") link.removeHttp() else name

                if (link != "") {
                    if (URLUtil.isValidUrl(link) || URLUtil.isValidUrl("http://$link")) {
                        link = if (URLUtil.isValidUrl(link)) link else "http://$link"
                        appRep.setFeatured(
                            FeaturedEntity(
                                label = name,
                                uri = link,
                                intent = Intent.ACTION_VIEW,
                                position = position
                            )
                        )
                    } else {
                        Toast.makeText(myView.context, myView.context.getString(R.string.invalid_link), Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(myView.context, myView.context.getString(R.string.empty_input), Toast.LENGTH_SHORT).show()
                }
            }
            setNegativeButton("CANCEL") { _, _ -> Unit }
            show()
        }
    }

    private fun checkContactsPermission() {
        if (ContextCompat.checkSelfPermission(
                view.context,
                android.Manifest.permission.READ_CONTACTS
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.READ_CONTACTS),
                INTENT_PICK_CONTACT
            )
        }
    }

    private fun FeaturedEntity.toFeaturedItem(): FeaturedItem {
        val item = FeaturedItem(
            label = this.label,
            appPackage = this.packagename,
            icon = null,
            intent = this.intent,
            uri = this.uri
        )

        if (this.packagename != null) {
            val info = pm.getApplicationInfo(this.packagename, 0)
            item.icon = info.loadIcon(pm)
        }
        return item
    }

    private fun FeaturedItem.toFeaturedEntity(position: Int?): FeaturedEntity {
        return FeaturedEntity(
            label = label,
            intent = intent,
            packagename = appPackage,
            uri = uri,
            position = position
        )
    }

    fun String.removeHttp(): String {
        return this.replace(Regex("^(https?://)?"), "")
    }
}

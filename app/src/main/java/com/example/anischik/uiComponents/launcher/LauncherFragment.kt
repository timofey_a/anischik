package com.example.anischik.uiComponents.launcher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.R
import com.example.anischik.adapter.ItemHolderType
import com.example.anischik.uiComponents.Pref
import com.yandex.metrica.YandexMetrica

class LauncherFragment : MyLauncherFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("launcher fragment launched")
        }
        return view
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup?): View =
        inflater.inflate(R.layout.layout_apps_fragment, container, false)

    override fun makeRecyclerView(): RecyclerView {
        val spanCount = if (prefs.getBoolean(Pref.TIGHT_LAYOUT, false))
            resources.getInteger(R.integer.layout_columns_tight)
        else resources.getInteger(R.integer.layout_columns_standart)

        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = GridLayoutManager(activity, spanCount)
        adapter.layoutType = ItemHolderType.LAUNCHER
        recyclerView.adapter = adapter
        return recyclerView
    }
}

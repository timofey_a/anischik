package com.example.anischik.uiComponents.launcher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.R
import com.example.anischik.adapter.ItemHolderType
import com.yandex.metrica.YandexMetrica

class ListFragment : MyLauncherFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("list fragment launched")
        }
        return view
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup?): View =
        inflater.inflate(R.layout.layout_apps_fragment, container, false)

    override fun makeRecyclerView(): RecyclerView {
        val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        divider.setDrawable(activity?.getDrawable(R.drawable.divider_horizontal_line)!!)

        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
        adapter.layoutType = ItemHolderType.LIST
        recyclerView.addItemDecoration(divider)
        return recyclerView
    }
}

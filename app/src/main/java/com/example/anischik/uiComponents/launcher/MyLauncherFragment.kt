package com.example.anischik.uiComponents.launcher

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.anischik.MyApplication
import com.example.anischik.R
import com.example.anischik.adapter.AppsAdapter
import com.example.anischik.database.AppRepository
import com.example.anischik.interfaces.AppStorage
import com.example.anischik.models.FeaturedEntity
import com.example.anischik.uiComponents.customs.MySnackbar
import com.yandex.metrica.YandexMetrica
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class MyLauncherFragment: Fragment() {


    val scope = CoroutineScope(Dispatchers.Default)

    lateinit var prefs: SharedPreferences
    private lateinit var appRepository: AppRepository

    companion object {
        lateinit var adapter: AppsAdapter
        lateinit var recyclerView: RecyclerView
    }
    protected lateinit var myView: View
    override fun getView(): View = myView

    protected abstract fun makeRecyclerView(): RecyclerView
    protected abstract fun inflateView(inflater: LayoutInflater, container: ViewGroup?): View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflateView(inflater, container)
        prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        appRepository = AppRepository(MyApplication.appContext)

        adapter = makeAdapter()
        recyclerView = makeRecyclerView()
        if (savedInstanceState != null) {
            adapter.reSort()
        }
        adapter.update()

        return view
    }

    private fun makeAdapter(): AppsAdapter {
        adapter = (activity as AppStorage).getAppsAdapter()
        adapter.onItemClick = ::onItemClick
        adapter.onItemLongClick = ::onItemLongClick
        return adapter
    }

    protected open fun onItemClick(view: View?, position: Int) {
        val packageName = adapter.currentList[position].appPackage
        val intent = this.view.context.packageManager.getLaunchIntentForPackage(packageName)
        adapter.currentList[position].launchAmount += 1
        adapter.currentList[position].lastStart = System.currentTimeMillis()
        scope.launch(Dispatchers.Default) {
            appRepository.incAppStarts(packageName)
            appRepository.setUsedAppTime(packageName, System.currentTimeMillis())
        }
        startActivity(intent)
    }

    protected open fun onItemLongClick(view: View?, position: Int): Boolean {
        val popupMenu = PopupMenu(myView.context, view)
        popupMenu.menuInflater.inflate(R.menu.item_popup_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_context_delete -> {
                    val packageUri = Uri.parse("package:${adapter.currentList[position].appPackage}")
                    val deleteApp = Intent(Intent.ACTION_DELETE, packageUri)
                    startActivity(deleteApp)
                }
                R.id.item_context_info -> {
                    val packageUri = Uri.parse("package:${adapter.currentList[position].appPackage}")
                    val intent =
                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageUri)
                    YandexMetrica.reportEvent("app info opened", adapter.currentList[position].label)
                    startActivity(intent)
                }
                R.id.item_context_to_home -> {
                    scope.launch(Dispatchers.Default) {
                        val cur = adapter.currentList[position]
                        val intent = myView.context.packageManager.getLaunchIntentForPackage(cur.appPackage)
                        val result = appRepository.setFeatured(
                            FeaturedEntity(
                                label = cur.label,
                                packagename = cur.appPackage,
                                intent = null,
                                uri = intent?.toUri(0)
                            )).await()
                        if (!result) {
                            withContext(Dispatchers.Main) {
                                Toast.makeText(
                                    myView.context,
                                    getString(R.string.unable_to_add_application),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
                R.id.item_context_frequency -> {
                    val amount = adapter.currentList[position].launchAmount
                    val snackbar = MySnackbar(getView(), "${getString(R.string.starts_amount)}: $amount", 3000)
                    val sbText =
                        snackbar.snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
                    sbText.gravity = Gravity.CENTER_HORIZONTAL
                    YandexMetrica.reportEvent("app frequency requested", mapOf(adapter.currentList[position].label to amount))
                    snackbar.show()
                }
            }
            true
        }
        popupMenu.show()
        return true
    }
}
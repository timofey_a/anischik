package com.example.anischik.uiComponents.welcome

import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.anischik.MyApplication
import com.example.anischik.NavigationMain
import com.example.anischik.R
import com.example.anischik.database.AppRepository
import com.example.anischik.models.FeaturedEntity
import com.example.anischik.uiComponents.Pref
import com.example.anischik.uiComponents.launcher.MyLauncherFragment
import com.example.anischik.uiComponents.welcome.fragments.WelcomeFragment1
import com.example.anischik.uiComponents.welcome.fragments.WelcomeFragment2
import com.example.anischik.uiComponents.welcome.fragments.WelcomeFragment3
import com.example.anischik.uiComponents.welcome.fragments.WelcomeFragment4
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WelcomePage : AppCompatActivity() {

    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        useDayThemeFirst()
        setContentView(R.layout.activity_welcome)

        if(savedInstanceState == null) {
            YandexMetrica.reportEvent("welcome page launched")
        }

        addBottomDots(0)

        findViewById<ViewPager>(R.id.view_pager).apply {
            adapter = slideAdapter
            addOnPageChangeListener(this@WelcomePage.pageChangeListener)
            currentItem = 0
        }

        if(savedInstanceState == null) {
            CoroutineScope(Dispatchers.Default).launch {
                val context = MyApplication.appContext
                val db = AppRepository(context)
                for (i in 0..14) {
                    val f = FeaturedEntity(null, null, null)
                    f.position = i
                    db.setFeatured(f)
                }
                val int = Intent(Intent.ACTION_MAIN)
                int.addCategory(Intent.CATEGORY_LAUNCHER)
                val appsInfos = context.packageManager.queryIntentActivities(int, 0)
                for (i in 0..4) {
                    val info = appsInfos[i]
                    val intent = context.packageManager.getLaunchIntentForPackage(info.activityInfo.packageName)
                    val f = FeaturedEntity (
                        label = info.loadLabel(context.packageManager).toString(),
                        packagename = info.activityInfo.packageName,
                        intent = null,
                        uri = intent?.toUri(0)
                    )
                    db.setFeatured(f)
                }
            }
        }

    }

    private val slideAdapter = object :
        FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment =
            when (position) {
                0 -> WelcomeFragment1()
                1 -> WelcomeFragment2()
                2 -> WelcomeFragment3()
                3 -> WelcomeFragment4()
                else -> WelcomeFragment1()
            }

        override fun getCount(): Int = 4
    }

    private val pageChangeListener = object:ViewPager.OnPageChangeListener {

        override fun onPageSelected(position: Int) {
            addBottomDots(position)
        }

        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    }

    private fun addBottomDots(currentPage: Int) {
        val dots: Array<TextView?> = arrayOfNulls(4)

        dotsLayout!!.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]?.text = Html.fromHtml("&#8226;")
            dots[i]?.textSize = 30f
            dots[i]?.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            dotsLayout?.addView(dots[i])
        }

            dots[currentPage]?.apply{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    setTextColor(getColor(R.color.colorPrimary))
                else
                    resources.getColor(R.color.colorPrimary)
            }
    }

    fun clickNext(view: View) {
        if(view_pager!!.currentItem > 2) {
            prefs.edit().putBoolean(Pref.FIRST_START, false).apply()
            finish()
            startActivity(Intent(this, NavigationMain::class.java))
        } else {
            view_pager!!.currentItem += 1
        }
    }

    override fun onBackPressed() {
        if (view_pager!!.currentItem > 0)
            view_pager.currentItem -= 1
    }


    private fun useDayThemeFirst() {
        val nightMode = prefs.getBoolean(Pref.DARK_MODE, false)
        if (nightMode)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}

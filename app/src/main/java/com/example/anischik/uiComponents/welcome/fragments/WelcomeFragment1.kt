package com.example.anischik.uiComponents.welcome.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.anischik.R
import com.yandex.metrica.YandexMetrica

class WelcomeFragment1 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("welcome page: fragment 1 launched")
        }
        return inflater.inflate(R.layout.fragment_welcome1, container, false)
    }


}

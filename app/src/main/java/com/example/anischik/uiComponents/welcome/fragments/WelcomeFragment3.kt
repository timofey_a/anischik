package com.example.anischik.uiComponents.welcome.fragments


import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.anischik.R
import com.example.anischik.uiComponents.Pref
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.fragment_welcome3.*
import kotlinx.android.synthetic.main.fragment_welcome3.view.*

class WelcomeFragment3 : Fragment() {

    private lateinit var myView:View
    override fun getView() = myView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        myView = inflater.inflate(R.layout.fragment_welcome3, container, false)

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("welcome page: fragment 3 launched")
        }

        makeButtonChecked()
        view.radio_group_theme.setOnCheckedChangeListener(::onCheck)

        view.button_light_border.setOnClickListener {
            view.button_light.isChecked = true
        }
        view.button_dark_border.setOnClickListener {
            view.button_dark.isChecked = true
        }

        return view
    }

    private fun onCheck(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.button_light -> {
                changeDayNight(false)
            }
            R.id.button_dark -> {
                changeDayNight(true)
            }
        }
    }

    private fun changeDayNight(isNight: Boolean) {
        val mode = if(isNight)
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_NO

        YandexMetrica.reportEvent("welcome page: day-night changed", if (isNight) "night" else "day")

        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        prefs.edit().putBoolean(Pref.DARK_MODE, isNight).apply()
        AppCompatDelegate.setDefaultNightMode(mode)
    }

    private fun makeButtonChecked() {

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            view.button_dark.isChecked = true
        }
        else {
            view.button_light.isChecked = true
        }
        makeBackgroundChecked()
    }

    private fun makeBackgroundChecked() {
        val (but1, but2) = if (button_light.isChecked)
                Pair(button_light_border, button_dark_border)
            else
                Pair(button_dark_border, button_light_border)

        but1.background =
            requireActivity().getDrawable(R.drawable.border_button_pressed)
        but2.background =
            requireActivity().getDrawable(R.drawable.border_button)
    }
}



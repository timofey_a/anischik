package com.example.anischik.uiComponents.welcome.fragments

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.anischik.R
import com.example.anischik.uiComponents.Pref
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.fragment_welcome4.*
import kotlinx.android.synthetic.main.fragment_welcome4.view.*

class WelcomeFragment4 : Fragment() {


    private var activity: AppCompatActivity? = null
    private lateinit var myView: View
    override fun getView() = myView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflater.inflate(R.layout.fragment_welcome4, container, false)

        if (savedInstanceState == null) {
            YandexMetrica.reportEvent("welcome page: fragment 4 launched")
        }

        val isTightLayout = PreferenceManager.getDefaultSharedPreferences(activity)
            .getBoolean(Pref.TIGHT_LAYOUT, false)
        if(isTightLayout)
            view.button_layout_tight.isChecked = true
        else
            view.button_layout_standart.isChecked = true
        makeBackgroundChecked()

        view.sub_text_button_standart.setOnClickListener {
            button_layout_standart.isChecked = true
        }

        view.sub_text_button_tight.setOnClickListener {
            button_layout_tight.isChecked = true
        }

        view.radio_group_layout.setOnCheckedChangeListener (::onCheck)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.activity = context as? AppCompatActivity
    }

    private fun onCheck(group: RadioGroup, id: Int) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val layoutTight = when (id) {
            R.id.button_layout_tight -> true
            else -> false
        }
        prefs?.edit()?.putBoolean(Pref.TIGHT_LAYOUT, layoutTight
            )?.apply()
        makeBackgroundChecked()
        YandexMetrica.reportEvent("welcome page: layout changed", if (layoutTight) "tight" else "standard")
    }

    private fun makeBackgroundChecked() {
        val (but1, but2) = if (view.button_layout_tight.isChecked)
            Pair(sub_text_button_tight, sub_text_button_standart)
        else
            Pair(sub_text_button_standart, sub_text_button_tight)

        but1.background =
            requireActivity().getDrawable(R.drawable.border_button_pressed)
        but2.background =
            requireActivity().getDrawable(R.drawable.border_button)
    }

}

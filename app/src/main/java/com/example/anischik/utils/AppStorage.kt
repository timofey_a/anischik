package com.example.anischik.utils

import com.example.anischik.adapter.AppsAdapter

interface AppStorage {
    fun getAppsAdapter(): AppsAdapter
}
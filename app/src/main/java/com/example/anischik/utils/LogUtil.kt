package com.example.anischik.utils

import android.util.Log
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

open class SnackbarCallbackLog : Snackbar.Callback() {
    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
        super.onDismissed(transientBottomBar, event)
        val logText = when (event) {
            BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_ACTION -> "dismissed via an action click"
            BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_CONSECUTIVE -> "dismissed from a new Snackbar"
            BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_MANUAL -> "dismissed via a call to dismiss()"
            BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_SWIPE -> "dismissed via a swipe"
            BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_TIMEOUT -> "dismissed via a timeout"
            else -> null
        }
        Log.i("anischik", "SnackBar $logText")
    }

}
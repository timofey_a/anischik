package com.example.anischik.utils

import android.content.res.Resources
import android.net.Uri
import com.example.anischik.R

fun Uri.fromPackageUri(): String = this.toString().substring(8)

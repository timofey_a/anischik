package com.example.anischik.utils.wallpaper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.icu.util.TimeUnit
import android.os.Environment
import android.util.Log
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.anischik.MyApplication
import com.example.anischik.NavigationMain.Companion.WALLPAPER_WORK_NAME
import com.example.anischik.R
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class WallpaperUtils {

    companion object {
        const val WALLPAPER_DIR = "wallpapers"
        const val WALLPAPER_NAME = "wallpaper"

        private val scope = CoroutineScope(Dispatchers.Default)


        @SuppressLint("SimpleDateFormat")
        fun addWallpaperWork(interval: Int) {
            val currentDate = Calendar.getInstance()
            val date = Calendar.getInstance()

            date.apply {
                set(Calendar.HOUR_OF_DAY, 0)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
            }

            while(date.before(currentDate)) {
                date.add(Calendar.MINUTE, interval)
            }

            val timeDiff = date.timeInMillis - currentDate.timeInMillis

            val periodicWork = PeriodicWorkRequestBuilder<WallpaperWorker>(
                interval.toLong(), java.util.concurrent.TimeUnit.MINUTES
            )
                .setInitialDelay(timeDiff, java.util.concurrent.TimeUnit.MILLISECONDS)
                .build()

            WorkManager.getInstance(MyApplication.appContext).enqueueUniquePeriodicWork(
                WALLPAPER_WORK_NAME,
                ExistingPeriodicWorkPolicy.REPLACE,
                periodicWork
            )

            val dateFormat = SimpleDateFormat("MM.dd HH:mm")
            Log.d(this::class.simpleName, "Wallpaper work is assigned to ${dateFormat.format(date.time)}")
        }


        suspend fun getWallpaper(source: String): BitmapDrawable =
            withContext(scope.coroutineContext) {
                var bitmap: Bitmap? = null
                try {
                    bitmap = Picasso.get()
                        .load(getDiscCacheFile(WALLPAPER_NAME))
                        .get()
                } catch (_: Exception) { }

                if (bitmap == null) {
                    try {
                        updateWallpaper(source).join()
                        return@withContext BitmapDrawable(
                            MyApplication.appContext.resources,
                            Picasso.get()
                                .load(getDiscCacheFile(WALLPAPER_NAME))
                                .get()
                        )
                    } catch (_: Exception) {
                        Log.e(WallpaperUtils::class.simpleName, "Wallpaper update is failed")
                        return@withContext BitmapDrawable(
                            MyApplication.appContext.resources,
                            Picasso.get()?.load(R.drawable.wallpaper)?.get()
                        )
                    }
                } else {
                    return@withContext BitmapDrawable(
                        MyApplication.appContext.resources,
                        bitmap
                    )
                }

            }

        fun updateWallpaper(source: String) =
            scope.launch {
                Log.d("11111", "source: $source")
                updateCache(
                    Picasso.get().load(source).memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.drawable.wallpaper).get()
                ).join()
            }



        private fun updateCache(bitmap: Bitmap?) = scope.launch {
            val file = getDiscCacheFile(WALLPAPER_NAME)
            val fOut = FileOutputStream(file)
            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.close()
        }

        private fun getDiscCacheFile(fileName: String): File {
            val path = MyApplication.appContext.getDir(WALLPAPER_DIR, Context.MODE_PRIVATE)
            return File(path, fileName)
        }
    }
}
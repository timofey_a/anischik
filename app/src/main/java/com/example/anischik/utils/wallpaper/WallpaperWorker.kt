package com.example.anischik.utils.wallpaper

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.anischik.service.DownloadWallpaperService

class WallpaperWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {

        Log.d("${this::class.simpleName}", "doWork() called")

        val intent = Intent(
            context,
            DownloadWallpaperService::class.java
        )
            .setAction(DownloadWallpaperService.ACTION_DOWNLOAD_WALLPAPERS)

        context.startService(intent)
        return Result.success()
    }
}
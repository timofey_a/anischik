package com.example.content_user_read_apps2

import android.annotation.SuppressLint
import android.content.ContentValues
import android.database.Cursor
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.sql.Date
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {


    companion object {
        val APPS_URI = Uri.parse("content://com.example.anischik.provider/apps")
        val LAST_APP_URI = Uri.parse("content://com.example.anischik.provider/app_last")
        const val COLUMN_PACKAGE = "packageName"
        const val COLUMN_LABEL = "appLabel"
        const val COLUMN_AMOUNT = "amount"
        const val COLUMN_USED = "lastUse"

        val  USER_INFO_URI = Uri.parse("content://com.example.anischik.provider/user_info")
        const val PHONE_NUMBER = "phoneNumber"
        const val TELEGRAM = "telegram"
        const val VK = "vk"
        const val MAIL = "mail"
        const val GITLAB = "gitlab"
        const val LOCATION = "location"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readPckgsBtn.setOnClickListener { requestPackages() }
        readLastPckgBtn.setOnClickListener { readLast() }
        readProfile.setOnClickListener { readUser() }
        editStarts.setOnClickListener { updateApp() }
    }

    private fun requestPackages(): Unit {
        try {
        val cursor = contentResolver.query(APPS_URI, null, null, null, null)
        val stringBuilder = StringBuilder().append("APPLICATIONS: \n")

        cursor?.use {
            while (it.moveToNext()) {
                stringBuilder.append(it.toAppString())
            }
        }
        tv.text = stringBuilder.toString()
        cursor?.close()
        } catch (_: Exception) {
            Toast.makeText(this, "app haven't access to read used apps", Toast.LENGTH_LONG).show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun readLast() {
        try {
            val cursor = contentResolver.query(LAST_APP_URI, null, null, null, null)
        if(cursor?.moveToNext() == true)
            tv.text = "LAST STARTED:\n${cursor.toAppString()}"
        cursor?.close()
        } catch (_: Exception) {
            Toast.makeText(this, "app haven't access to read last app", Toast.LENGTH_LONG).show()
        }
    }

    private fun readUser() {
        try {
            val cursor = contentResolver.query(USER_INFO_URI, null, null, null, null)
            val builder = StringBuilder()
            cursor?.let {
                if (it.moveToNext()) {
                    builder
                        .append("PROFILE:\n")
                        .append("phone number: ")
                        .append(it.getString(it.getColumnIndex(PHONE_NUMBER)))
                        .append("\ntelegram: ")
                        .append(it.getString(it.getColumnIndex(TELEGRAM)))
                        .append("\nvk: ")
                        .append(it.getString(it.getColumnIndex(VK)))
                        .append("\nmail: ")
                        .append(it.getString(it.getColumnIndex(MAIL)))
                        .append("\ngitlab: ")
                        .append(it.getString(it.getColumnIndex(GITLAB)))
                        .append("\nlocation: ")
                        .append(it.getString(it.getColumnIndex(LOCATION)))
                }
            }
            tv.text = builder.toString()
            cursor?.close()
        } catch (_: Exception) {
            Toast.makeText(this, "app haven't access to read user info", Toast.LENGTH_LONG).show()
        }
    }

    fun updateApp() {

        val builder = AlertDialog.Builder(this)
        val frame = LinearLayout(this)
        frame.orientation = LinearLayout.VERTICAL
        val inputP = EditText(this)
        inputP.hint = "package name"
        val inputV = EditText(this)
        inputV.hint = "new amount"
        frame.addView(inputP)
        frame.addView(inputV)

        builder.apply {
            setTitle("edit starts amount")
            setView(frame)
            setPositiveButton("CHANGE") { _, _ ->
                if ((inputP.text.toString() != "") and (inputV.text.toString() != "")) {
                    try {
                        updateAppCount(inputP.text.toString(), inputV.text.toString())
                    } catch (_: Exception) {
                        Toast.makeText(this@MainActivity, "error: can't update", Toast.LENGTH_LONG).show()
                        return@setPositiveButton
                    }

                    Toast.makeText(this@MainActivity, "app data changed", Toast.LENGTH_LONG).show()

                } else {
                    Toast.makeText(this@MainActivity, "empty input", Toast.LENGTH_LONG).show()
                }
            }
                .setNegativeButton("CANCEL") { _, _ -> Unit}
        }
            .show()
    }

    fun updateAppCount(packageName: String, amount: String): Int {
        val entity = ContentValues()
        entity.put(COLUMN_PACKAGE, packageName)
        entity.put(COLUMN_AMOUNT, amount)
        return contentResolver.update(APPS_URI, entity, null, null)
    }

    private fun Cursor.toAppString(): StringBuilder {
        val stringBuilder = StringBuilder()
        val appPackage = getString(getColumnIndex(COLUMN_PACKAGE))
        val used = Date(getLong(getColumnIndex(COLUMN_USED)))
        val appLabel = getString(getColumnIndex(COLUMN_LABEL))
        val appStarts = getLong(getColumnIndex(COLUMN_AMOUNT))

        val dateFormat = SimpleDateFormat("MM.dd HH:mm")

        return stringBuilder.append("package: $appPackage\n" )
            .append("label: $appLabel\n")
            .append("starts amount: $appStarts\n")
            .append("used: ${dateFormat.format(used)}\n\n")
    }
}
